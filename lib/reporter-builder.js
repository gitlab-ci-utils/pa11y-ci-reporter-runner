'use strict';

const path = require('node:path');

const reporterMethods = ['beforeAll', 'begin', 'results', 'error', 'afterAll'];
const noop = () => {};

/* eslint-disable node/global-require -- load models pa11y-ci behavior for loading
   user-provided reporters */
const loadReporter = (reporterName) => {
    try {
        // nosemgrep: eslint.detect-non-literal-require
        return require(reporterName);
    } catch {
        // nosemgrep: eslint.detect-non-literal-require
        return require(path.resolve(process.cwd(), reporterName));
    }
};
/* eslint-enable node/global-require -- load models pa11y-ci behavior for loading
   user-provided reporters */

/**
 * Creates a reporter object for the specified path, following the logic
 * used by pa11y-ci for reporter generation. Unlike pa11y-ci, ensures a
 * function exists for each reporter event to allow manual execution.
 *
 * @param   {string}    reporterName Name of the reporter to execute
 *                                   (module or path).
 * @param   {object}    options      The reporter options object.
 * @param   {object}    config       The Pa11y CI configuration.
 * @returns {object}                 The reporter object.
 * @throws  {TypeError}              Throws if reporterName is not a string.
 */
// eslint-disable-next-line complexity -- allow low threshold
const buildReporter = (reporterName, options, config) => {
    if (typeof reporterName !== 'string') {
        throw new TypeError('reporterName must be a string');
    }

    try {
        let reporter = loadReporter(reporterName);
        if (typeof reporter === 'function') {
            reporter = reporter(options, config);
        }
        for (const method of reporterMethods) {
            // Value is internal object state
            // nosemgrep: eslint.detect-object-injection
            if (typeof reporter[method] !== 'function') {
                // Value is internal object state
                // nosemgrep: eslint.detect-object-injection
                reporter[method] = noop;
            }
        }
        return reporter;
    } catch (error) {
        throw new Error(`Error loading reporter: ${error.message}`);
    }
};

module.exports.buildReporter = buildReporter;
