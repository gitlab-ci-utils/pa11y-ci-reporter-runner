'use strict';

/**
 * State machine for pa11yci.
 *
 * @module pa11yci-machine
 */

const { createMachine, assign } = require('xstate');

/**
 * State machine for pa11yci.
 */
const pa11yciMachine = createMachine(
    {
        context: ({ input }) => ({
            urlIndex: input.urlIndex,
            urls: input.urls
        }),
        id: 'pa11yci-runner',
        initial: 'init',
        // Opting in for v4, will be default in xstate v5
        predictableActionArguments: true,
        // Opting in for v4, will be default in xstate v5
        preserveActionOrder: true,
        states: {
            /* eslint-disable sort-keys -- order by sequence */
            init: {
                entry: 'setInitialUrlIndex',
                on: {
                    NEXT: { target: 'beforeAll' }
                }
            },
            beforeAll: {
                on: {
                    NEXT: [
                        { target: 'beginUrl', guard: 'hasUrls' },
                        { target: 'afterAll' }
                    ],
                    RESET: { target: 'init' }
                }
            },
            beginUrl: {
                on: {
                    NEXT: { target: 'urlResults' },
                    RESET: { target: 'init' }
                }
            },
            urlResults: {
                on: {
                    NEXT: [
                        { target: 'afterAll', guard: 'isLastUrl' },
                        { target: 'beginUrl', actions: 'incrementUrl' }
                    ],
                    RESET: { target: 'init' }
                }
            },
            afterAll: {
                on: {
                    RESET: { target: 'init' }
                }
            }
            /* eslint-enable sort-keys -- order by sequence */
        }
    },
    {
        actions: {
            incrementUrl: assign({
                urlIndex: ({ context }) => context.urlIndex + 1
            }),
            setInitialUrlIndex: assign({
                urlIndex: () => 0
            })
        },
        guards: {
            hasUrls: ({ context }) => context.urls.length > 0,
            isLastUrl: ({ context }) =>
                context.urlIndex === context.urls.length - 1
        }
    }
);

module.exports = pa11yciMachine;
