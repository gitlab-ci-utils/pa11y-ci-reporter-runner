# Changelog

## v5.0.5 (2025-01-17)

### Fixed

- Updated to latest dependencies (`xstate@5.19.2`)

## v5.0.4 (2025-01-06)

### Fixed

- Updated to latest dependencies (`xstate@5.19.1`)

## v5.0.3 (2024-10-27)

### Fixed

- Updated to latest dependencies (`xstate@5.18.2`)

## v5.0.2 (2024-08-25)

### Fixed

- Updated to latest dependencies (`xstate@5.17.4`)

## v5.0.1 (2024-06-14)

### Fixed

- Updated to latest dependencies (`xstate@5.13.2`)

### Miscellaneous

- Update to Renovate config v1.1.0.

## v5.0.0 (2024-05-06)

### Changed

- BREAKING: Deprecated support for Node 21 (end-of-life 2024-06-01) and added
  support for Node 22 (released 2024-04-25). Compatible with all current and
  LTS releases (`^18.12.0 || ^20.9.0 || >=22.0.0`). (#43)

### Fixed

- Updated to latest dependencies (`xstate@5.13.0`)

## v4.1.3 (2024-04-17)

### Fixed

- Updated to latest dependencies (`xstate@5.11.0`)

## v4.1.2 (2024-04-09)

### Fixed

- Updated to latest dependencies (`xstate@5.10.0`)

## v4.1.1 (2024-03-04)

### Fixed

- Updated to latest dependencies (`xstate@5.9.1`)

### Miscellaneous

- Updated Renovate config to use use new [presets](https://gitlab.com/gitlab-ci-utils/renovate-config)
  project. (#41)

## v4.1.0 (2024-01-11)

### Changed

- Updated to `xstate@5.5.1`, which included breaking changes requiring
  refactoring of the state machine and state service, as well as an
  [`xstate` change](https://github.com/statelyai/xstate/pull/4596). (#39)

## v4.0.0 (2023-11-03)

### Changed

- BREAKING: Deprecated support for Node 16 (end-of-life 2023-09-11) and added
  support for Node 21 (released 2023-10-17). Compatible with all current and
  LTS releases (`^18.12.0 || >=20.0.0`). (#)

### Fixed

- Updated to latest dependencies (`xstate@4.38.3`)

### Miscellaneous

- Updated package to publish to NPM with
  [provenance](https://github.blog/2023-04-19-introducing-npm-package-provenance/).

## v3.0.1 (2023-08-13)

### Fixed

- Updated to latest dependencies (`xstate@4.38.2`)

## v3.0.0 (2023-06-07)

### Changed

- BREAKING: Deprecated support for Node 14 (end-of-life 2023-04-30) and 19
  (end-of-life 2023-06-01). Compatible with all current and LTS releases
  (^16.13.0 || ^18.12.0 || >=20.0.0). (#34)
- BREAKING: Added TypeScript type declarations. (#35)

### Fixed

- Updated to latest dependencies (`xstate@4.37.2`)

## v2.0.5 (2023-02-26)

### Fixed

- Updated to latest dependencies (`xstate@4.37.0`)

## v2.0.4 (2022-12-24)

### Fixed

- Updated to latest dependencies (`xstate@4.35.1`)

## v2.0.3 (2022-11-06)

### Fixed

- Updated to latest dependencies (`xstate@4.34.0`)

### Miscellaneous

- Updated pipeline to have long-running jobs use GitLab shared `medium` sized runners. (#31)

## v2.0.2 (2022-09-02)

### Fixed

- Updated `xstate` actions configuration based on latest recommendations, eliminating execution warnings. (#30)
- Updated to latest dependencies (`xstate@4.33.5`)

## v2.0.1 (2022-08-07)

### Fixed

- Updated to latest dependencies (`xstate@4.33.0`)

## v2.0.0 (2022-05-31)

### Changed

- BREAKING: Deprecated support for Node 12 and 17 since end-of-life. Compatible with all current and LTS releases (`^14.15.0 || ^16.13.0 || >=18.0.0`). (#27, #28)

### Fixed

- Updated tests for output changes in `pa11y-ci-reporter-cli-summary` v1.1.0
- Updated to latest dependencies, including resolving CVE-2021-43138 and CVE-2021-44906 (both dev only)

### Miscellaneous

- Added `prettier` and disabled `eslint` formatting rules. (#26)
- Added test coverage threshold requirements (#29)

## v1.0.1 (2022-03-18)

### Fixed

- Fixed issue where `reset()` was not re-initializing the reporter, so any reporter state was persisted. (#24)

### Miscellaneous

- Updated README for clarification (#23)

## v1.0.0 (2022-03-13)

### Changed

- Added `runUntilNext` function, similar to `runUntil`, that runs to a target state and URL, but stops at the state prior to the target (so `runNext` would execute the target). (#15)
- Added functions to get runner state (`getCurrentState` and `getNextState`) (#15)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Updated runner state service to use a custom `xstate` interpreter for finer control. (#15)

## v0.7.1 (2022-03-05)

### Fixed

- Update README with note that mermaid charts are not rendered on npm and can be viewed in the repository (#19)
- Updated to latest dependencies

## v0.7.0 (2022-03-03)

### Changed

- BREAKING: Change `createRunner` factory function from the default to a named export and added export of `RunnerStates` enum. (#18)
- Added runner execution commands `runNext` that runs the next state and `runUntil` that runs to a given state and URL. (#13, #16, #17)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Updated documentation to detail runner state machine, runner execution, and additional examples

## v0.6.0 (2022-02-24)

### Changed

- BREAKING: Updated runner to throw if `runAll` is called from the `afterAll` state (e.g. called multiple times in succession). Added a `reset` function that must be called first to reset the runner to the initial state. (#12)
- Incorporated `xstate` for runner state management (#12)

### Fixed

- Fixed integration tests to properly render color output in all environments (#8)

## v0.5.0 (2022-02-01)

Initial release
