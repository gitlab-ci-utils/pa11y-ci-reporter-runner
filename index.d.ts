type RunnerStates = 'afterAll' | 'beforeAll' | 'beginUrl' | 'urlResults';

export type RunnerState = {
  state: RunnerStates;
  url: string;
};

export type Runner = {
  /**
   * Get the current state (state, url).
   *
   * @instance
   * @returns The current runner state.
   * @public
   */
  getCurrentState(): RunnerState;

  /**
   * Get the next state (state, url).
   *
   * @instance
   * @returns The next runner state.
   * @public
   */
  getNextState(): RunnerState;

  /**
   * Resets the runner and reporter to the initial states.
   *
   * @instance
   * @async
   * @public
   */
  reset(): void;

  /**
   * Executes the entire Pa11y CI sequence, calling all reporter functions.
   *
   * @instance
   * @async
   * @public
   */
  runAll(): void;

  /**
   * Executes the next event in the Pa11y CI sequence, calling the
   * appropriate reporter function.
   *
   * @instance
   * @async
   * @public
   */
  runNext(): void;

  /**
   * Executes the entire Pa11y CI sequence, calling all reporter functions,
   * until the specified current state and optional URL are reached. If a URL is not
   * specified, the run completes on the first occurrence of the target state.
   *
   * @instance
   * @param targetState The target state to run to.
   * @param targetUrl The target URL to run to.
   * @async
   * @public
   */
  runUntil(state: RunnerStates, url?: string): void;

  /**
   * Executes the entire Pa11y CI sequence, calling all reporter functions,
   * until the specified next state and optional URL are reached. If a URL is not
   * specified, the run completes on the first occurrence of the target state.
   *
   * @instance
   * @param targetState The target state to run to.
   * @param targetUrl The target URL to run to.
   * @async
   * @public
   */
  runUntilNext(state: RunnerStates, url?: string): void;
};

/**
 * Factory to create a pa11y-ci reporter runner that can execute
 * a reporter with the specified pa11y-ci JSON results file.
 *
 * @param   resultsFileName Pa11y CI JSON results file.
 * @param   reporterName    Name of the reporter to execute (module or path).
 * @param   options         The reporter options.
 * @param   config          The Pa11y CI configuration.
 * @returns                 A Pa11y CI reporter runner.
 * @static
 * @public
 */
export function createRunner(
  resultsFileName: string,
  reporterName: string,
  options?: object,
  config?: object
): Runner;
