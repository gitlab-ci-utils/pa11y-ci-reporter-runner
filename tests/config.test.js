'use strict';

const createConfig = require('../lib/config');
const defaultConfig = require('../lib/default-config');

describe('config factory', () => {
    it('should export a factory function', () => {
        expect.assertions(1);
        expect(typeof createConfig).toBe('function');
    });

    describe('config', () => {
        it('should return defaults with default config if no config provided', () => {
            expect.assertions(1);
            const config = {};

            const pa11yciConfig = createConfig(config);

            expect(pa11yciConfig.defaults).toStrictEqual(defaultConfig);
        });

        it('should return defaults with provided config overriding default config', () => {
            expect.assertions(1);
            const config = {
                defaults: { wrapWidth: 50 }
            };
            const expectedConfig = { ...defaultConfig, ...config.defaults };

            const pa11yciConfig = createConfig(config);

            expect(pa11yciConfig.defaults).toStrictEqual(expectedConfig);
        });

        describe('get config for url', () => {
            const testConfig = {
                defaults: {
                    timeout: 30_000
                },
                urls: [
                    'https://pa11y.org',
                    {
                        timeout: 5000,
                        url: 'https://somewhere.org'
                    }
                ]
            };

            const testUrlConfig = (config, url, expectedUrlConfig) => {
                expect.assertions(1);

                const pa11yciConfig = createConfig(config);
                const urlConfig = pa11yciConfig.getConfigForUrl(url);

                expect(urlConfig).toStrictEqual(
                    expectedUrlConfig || pa11yciConfig.defaults
                );
            };

            it('should return defaults if urls are not specified in config', () => {
                expect.hasAssertions();
                const config = { ...testConfig.defaults };
                testUrlConfig(config, testConfig.urls[0]);
            });

            it('should return defaults if empty url array is specified in config', () => {
                expect.hasAssertions();
                const config = { ...testConfig.defaults, urls: [] };
                testUrlConfig(config, testConfig.urls[0]);
            });

            it('should return defaults if given url is not in urls', () => {
                expect.hasAssertions();
                const config = testConfig;
                testUrlConfig(config, 'https://nowhere.org');
            });

            it('should return defaults if url is in urls as string', () => {
                expect.hasAssertions();
                const config = testConfig;
                testUrlConfig(config, testConfig.urls[0]);
            });

            it('should return config with url config overriding defaults', () => {
                expect.hasAssertions();
                const config = testConfig;
                const expectedConfig = {
                    ...defaultConfig,
                    ...testConfig.defaults,
                    ...testConfig.urls[1]
                };
                testUrlConfig(config, testConfig.urls[1], expectedConfig);
            });
        });
    });
});
