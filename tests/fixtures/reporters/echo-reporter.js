'use strict';

/**
 * Reporter that logs arguments passed to all functions, saves to
 * JSON file, and optionally outputs to stdout. All reporter
 * functions are async.
 *
 * @module echo-reporter
 */

const fs = require('node:fs');

/**
 * JSON transformer for Pa11y CI results. Converts Error to
 * message and removed the browser object.
 *
 * @param   {object} key   The object key being serialized.
 * @param   {object} value The object value to be serialized.
 * @returns {object}       The transformed value to be serialized.
 * @private
 */
const jsonReplacer = (key, value) => {
    // If error object replace with message, otherwise returns error name.
    if (value instanceof Error) {
        return { message: value.message };
    }

    // The browser object causes circular reference errors when serializing, so remove.
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/Cyclic_object_value
    if (key === 'browser') {
        return { removed: 'The browser object was removed' };
    }

    return value;
};

/**
 * Asynchronously serializes the object to JSON and logs to console per reporter options.
 *
 * @param   {object}  value   The value to be logged.
 * @param   {object}  options The reporter options.
 * @returns {Promise}         Promise that resolves when logging complete.
 * @private
 */
const log = (value, options = {}) => {
    const timeout = 5;
    // eslint-disable-next-line promise/avoid-new -- promisifying setTimeout
    return new Promise((resolve) => {
        setTimeout(() => {
            if (options.logConsole) {
                console.log(JSON.stringify(value, jsonReplacer));
            }
            resolve();
        }, timeout);
    });
};

/**
 * Pa11y CI reporter that logs arguments passed to all functions,
 * saves to JSON file, and optionally outputs to stdout.
 *
 * @param   {object}  options            Reporter options object.
 * @param   {boolean} options.logConsole Write serialized args to stdout
 *                                       instead of file.
 * @param   {object}  config             Pa11y CI config.
 * @returns {object}                     Pa11y CI echo reporter.
 * @public
 */
// eslint-disable-next-line max-lines-per-function -- factory function with state
const reporter = (options, config) => {
    const reporterArguments = {
        afterAll: {},
        beforeAll: {},
        begin: [],
        error: [],
        init: {
            config,
            options
        },
        results: []
    };

    return {
        /* eslint-disable sort-keys -- execution sequence */
        beforeAll: async (urls) => {
            const args = { urls };
            reporterArguments.beforeAll = args;
            await log(args, options);
        },

        begin: async (url) => {
            const args = { url };
            reporterArguments.begin.push(args);
            await log(args, options);
        },

        // eslint-disable-next-line no-shadow -- using API names
        results: async (results, config) => {
            const args = { config, results };
            reporterArguments.results.push(args);
            await log(args, options);
        },

        // eslint-disable-next-line no-shadow -- using API names
        error: async (error, url, config) => {
            const args = { config, error, url };
            reporterArguments.error.push(args);
            await log(args, options);
        },

        // eslint-disable-next-line no-shadow -- using API names
        afterAll: async (report, config) => {
            const args = { config, report };
            reporterArguments.afterAll = args;
            await log(args, options);

            if (!options.logConsole) {
                fs.writeFileSync(
                    'echo-reporter-results.json',
                    JSON.stringify(reporterArguments, jsonReplacer)
                );
            }
        }
        /* eslint-enable sort-keys -- execution sequence */
    };
};

module.exports = reporter;
