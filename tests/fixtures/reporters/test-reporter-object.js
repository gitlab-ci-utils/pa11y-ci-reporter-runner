'use strict';

/**
 * Reporter that implements afterAll function for testing, returning an object.
 *
 * @module test-reporter-object
 */

/**
 * Reporter afterAll function that shows a summary of errors.
 *
 * @param {object} results Pa11y-CI test results.
 * @public
 */
const afterAll = (results) => {
    console.log(`afterAll - ${results.errors} errors`);
};

module.exports.afterAll = afterAll;
