'use strict';

const { createRunner, RunnerStates } = require('../../');

const resultsFileName = './tests/fixtures/results/pa11yci-results-valid.json';
const reporterName = 'tests/fixtures/reporters/counter-reporter.js';
// Const reporterName = 'tests/fixtures/reporters/test-reporter-function.js';
// Const reporterName = 'tests/fixtures/reporters/test-reporter-object.js';
// Const reporterName = 'pa11y-ci-reporter-cli-summary';
// Const reporterName = 'tests/fixtures/reporters/echo-reporter.js';
// Const options = { foo: 1, bar: 'baz' };
// Const options = { logConsole: true };
const options = {};
const config = {
    defaults: {
        timeout: 30_000,
        viewport: {
            height: 768,
            width: 1024
        }
    },
    urls: [
        'somewhere-no-issues.html',
        'somewhere-with-issues.html',
        {
            timeout: 50,
            url: 'somewhere-error.html',
            viewport: {
                isMobile: true
            }
        }
    ]
};

const runner = createRunner(resultsFileName, reporterName, options, config);

(async () => {
    // Await runner.runAll();
    // await runner.runUntil(RunnerStates.afterAll);
    await runner.runUntil(RunnerStates.afterAll);
    await runner.reset();
    await runner.runUntil(RunnerStates.urlResults);
})();
