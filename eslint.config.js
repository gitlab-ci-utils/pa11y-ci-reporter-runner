'use strict';

const globals = require('globals');
const recommendedConfig = require('@aarongoldenthal/eslint-config-standard/recommended');

module.exports = [
    ...recommendedConfig,
    {
        files: ['tests/**/*.js'],
        languageOptions: {
            globals: {
                ...globals.jest
            }
        },
        name: 'jest helpers'
    },
    {
        files: ['**/*.js'],
        name: 'FIX ME',
        rules: {
            'promise/no-multiple-resolved': 'off'
        }
    },
    {
        ignores: ['.vscode/**', 'archive/**', 'node_modules/**', 'coverage/**'],
        name: 'ignores'
    }
];
